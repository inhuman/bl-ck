Crafty.c "Block",
  init: ->
    @addComponent "2D"
    @_template = [[{x:0,y:0},{x:0,y:1},{x:1,y:0},{x:-1,y:0}],
                [{x:0,y:0},{x:-1,y:0},{x:0,y:1},{x:0,y:2}],
                [{x:0,y:0},{x:1,y:0},{x:0,y:1},{x:1,y:1}]]

    Crafty.bind "Turn", (key) =>
      @y += @h
      for tile in @_children
        if tile.hit "Tile"
          @y -= @h
          tile.spawn 10, 2, 5
          break;

  template: (form) ->
    unless form?
      random = Math.floor(Math.random() * @_template.length)
      form = @_template[random]
    for spec in form
      tile = Crafty.e("Tile").attr
        x: @x + @w*spec.x
        y: @y + @h*spec.y
        w: @w-1
        h: @h-1
      tile.color "blue"
      @attach tile
    @