Crafty.c "CounterAttack",
  init: ->
    @minDamage = 0
    @maxDamage = 0
    @bind "Hit", ({damage, target}) ->
      counter = Crafty.math.randomInt @minDamage, @maxDamage
      target.trigger "Hit", {damage: counter, target: @}

  counterAttack: (@minDamage, @maxDamage) -> @
