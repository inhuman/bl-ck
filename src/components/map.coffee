Crafty.c "Map",
  init: ->
    @requires "2D, Canvas, Color"
    @color "#888"
    @tileData = {}
    moves = 0

    testTemplate = ({x: col, y:0} for col in [0..14] when col != 6)

    Crafty.bind "Turn", (key) =>
      if moves % 10 == 0
        x = Math.floor(Math.random() * 15) * @tileWidth
        x = 0
        block = Crafty.e("Block, Draggable").attr
          x: x
          y: 0
          w: @tileWidth
          h: @tileHeight

        if moves < 31
          block.template(testTemplate)
        else
          pipeTpl = ({x: 6, y:row} for row in [0..3])
          block.template(pipeTpl)

      # Scan for full row to remove.
      @scanMap()
      @scanRows()
      @scanCols()
      moves = moves + 1

  generate: (@x, @y, @cols, @rows, @tileWidth, @tileHeight)->
    @w = cols * @tileWidth
    @h = rows * @tileHeight
    for col in [0..cols-1]
      Crafty.e("Tile").attr
        x: col*@tileWidth
        y: @h-@tileHeight
        w: @tileWidth-1
        h: @tileHeight-1
      .color "blue"
    @

  scanRows: ->
    for row in [0..@rows-2]
      completed = false
      for col in [0..@cols-1]
        unless @tileData["#{col*@tileWidth},#{row*@tileHeight}"]?
          completed = false
          break
        completed = true
      if completed
        foundEntities = Crafty.map.search {_x:0,_y:@tileWidth*row,_w:@tileHeight*@cols,_h:@tileHeight}
        for entity in foundEntities when entity.has "Tile"
          entity.destroy()
          @tileData["#{entity.x},#{entity.y}"] = undefined


  scanCols: ->
    for col in [0..@cols-1]
      spottedEmpty = false
      for row in [@rows-2..0] by -1
        if !@tileData["#{col*@tileWidth},#{row*@tileHeight}"]?
          spottedEmpty = true
        else if spottedEmpty
          spottedEmpty = false



  scanMap: ->
    entities = Crafty.map.search {_x:0,_y:0,_w:@cols*@tileWidth,_h:@rows*@tileHeight}
    @tileData = {}
    for entity in entities when entity.has "Tile"
      @tileData["#{entity.x},#{entity.y}"] = entity
