Crafty.c "HP",
  init: ->
    @_hp = 0

    @bind "Hit", ({damage}) ->
      @_hp -= damage
      @destroy() if @_hp <= 0

  hp: (amount) ->
    @_hp = amount
    @