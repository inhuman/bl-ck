Crafty.c "Tile",
  init: ->
    @addComponent "2D, Canvas, Color, Collision"

  spawn: (hp, minDamage, maxDamage) ->
    return if @monster?
    @monster = Crafty.e("2D, Canvas, Color, Enemy, HP, CounterAttack").attr
        x: @x
        y: @y
        w: @w
        h: @h
      .color("red")
      .hp(hp)
      .counterAttack(minDamage, maxDamage)
      .bind "Remove", =>
        @destroy()