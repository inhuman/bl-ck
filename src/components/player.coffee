{UP_ARROW, DOWN_ARROW, LEFT_ARROW, RIGHT_ARROW, W, S, A, D} = Crafty.keys

Crafty.c "Player",
  init: ->
    @addComponent "2D, Canvas, Collision, HP"
    @z = 1000
    @_movement = {}
    @_movement[W] = {x: 0, y: -1}
    @_movement[S] = {x: 0, y: 1}
    @_movement[A] = {x: -1, y: 0}
    @_movement[D] = {x: 1, y: 0}

  movement: (amount) ->
    @bind "KeyDown", ({key}) ->
      direction = @_movement[key]
      return unless direction?

      moveX = amount*direction.x
      moveY = amount*direction.y

      @x += moveX
      @y += moveY

      # @collision ([moveX, moveY], [@w+moveX,moveY], [@w+moveX,@h+moveY], [moveX,@h+moveY])
      hitEnemy = @hit("Enemy")
      if hitEnemy or not @hit("Tile")
        @x -= moveX
        @y -= moveY

        if hitEnemy?
          target = hitEnemy[0]?.obj
          target.trigger "Hit", {damage:5, target:@} if target?

      else
        Crafty.trigger "Turn", key

