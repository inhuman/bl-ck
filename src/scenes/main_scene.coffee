{UP_ARROW, DOWN_ARROW, LEFT_ARROW, RIGHT_ARROW, W, S, A, D} = Crafty.keys
{LEFT, MIDDLE, RIGHT} = Crafty.mouseButtons

MAPROWS = 20
MAPCOLS = 15
TILEWIDTH = 32
TILEHEIGHT = 32

Crafty.scene "Main", ->
  map = Crafty.e("Map").generate 0, 0, MAPCOLS, MAPROWS, TILEWIDTH, TILEHEIGHT

  player = Crafty.e("Player, Color").attr
    x: 160
    y: map.h - TILEHEIGHT
    w: TILEWIDTH
    h: TILEHEIGHT
  .color("#fff")
  .movement(TILEWIDTH)
  .hp(20)
